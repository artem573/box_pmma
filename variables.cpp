#include "variables.h"

double us;
double rs;
int Ns;

std::vector<double> r, r_f;
std::vector<double> alpha;

std::vector<double> rho, C, lambda, T, T0;

std::vector<double> t_T;
std::vector<double> T_box;
std::vector<double> t_W;
std::vector<double> W_s;

std::vector<double> Q, W;

std::vector<double> a, b, c, d;
std::vector<double> lambda_f;
