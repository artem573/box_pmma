#include "solve.h"

void interp_lambda()
{
  lambda_f[0] = lambda[0];
  for(int i = 1; i <= Nr; ++i)
    {
      double lambda_l = lambda[i-1];
      double lambda_r = lambda[i];
      
      lambda_f[i] = 2*lambda_l*lambda_r/(lambda_l + lambda_r);
    }
  lambda_f[Nr+1] = lambda[Nr];
}

void set_coeffs()
{
  for(int i = 1; i < Nr; ++i)
    {
      a[i] = lambda_f[i]   / sqr(dr) * sqr(r_f[i])   / sqr(r[i]);
      c[i] = lambda_f[i+1] / sqr(dr) * sqr(r_f[i+1]) / sqr(r[i]);
      b[i] = a[i] + c[i] + rho[i]*C[i]/dt;
      d[i] = rho[i]*C[i]*T0[i]/dt + rho[i]*W[i]*Q[i];
    }
}

void progonka()
{
  std::vector<double> P(Nr+1);
  std::vector<double> Q(Nr+1);
  
  P[0] = -1;
  Q[0] = 0;

  for(int i = 1; i < Nr; ++i)
    {
      P[i] = -c[i]/(b[i] + a[i]*P[i-1]);
      Q[i] = (d[i] + a[i]*Q[i-1])/(b[i] + a[i]*P[i-1]);
    }

  for(int i = Nr-1; i >= 0; --i)
    {
      T[i] = Q[i] - P[i]*T[i+1];
    }
}

double sqr(const double a)
{
  return a*a;
}

void init()
{
  r.resize(Nr+1);
  alpha.resize(Nr+1,0);
  r_f.resize(Nr+2);
  
  for(int i = 0; i <= Nr; ++i)
    {
      r[i] = i*r_box/Nr;
    }

  r_f[0] = r[0];
  for(int i = 1; i <= Nr; ++i)
    {
      r_f[i] = 0.5*(r[i-1] + r[i]);
    }
  r_f[Nr+1] = r[Nr];

  rs = r_f[Ns0];
  Ns = Ns0;
  
  rho.resize(Nr+1, rho_s);
  C.resize(Nr+1, C_s);
  lambda.resize(Nr+1, lambda_s);
  T.resize(Nr+1, T_box[0]);
  T0.resize(Nr+1, T_box[0]);

  a.resize(Nr+1);
  b.resize(Nr+1);
  c.resize(Nr+1);
  d.resize(Nr+1);

  lambda_f.resize(Nr+2);

  Q.resize(Nr+1, 0);
  W.resize(Nr+1, 0);

  for(int i = 0; i <= Nr; ++i)
    {
      i <= Ns0 ? alpha[i] = 1 : alpha[i] = 0;
    }
  
  update_thermo();
}

void update_thermo()
{
  /*
  // Constant thermophysics
  for(int i = 0; i <= Nr; ++i)
    {
      if(alpha[i] > 0.5)
        {
          Q[i] = Q_s;
          rho[i] = rho_s;
          C[i] = C_s;
          lambda[i] = lambda_s;
        }
      else
        {
          Q[i] = 0;
          rho[i] = rho_g;
          C[i] = C_g;
          lambda[i] = lambda_g;
        }
    }
  */

  // Variable thermophysics
  for(int i = 0; i <= Nr; ++i)
    {
      if(alpha[i] > 0.5)
        {
          Q[i] = Q_s;
          rho[i] = rho_s;
          C[i] = C_s;
          lambda[i] = lambda_s;
        }
      else
        {
          rho[i] = p0/(R_N2*T_box[0]);
          
          C[i] = Cp_calc({ T[i], 1000, R_N2,
                2.9266400E+0, 1.4879768E-3, -5.6847600E-7, 1.0097038E-10, -6.7533510E-15,
                3.2986770E+0, 1.4082404E-3, -3.9632220E-6, 5.6415150E-9,  -2.4448540E-12 });
          
          lambda[i] = lambda_calc({ T[i], 1000,
                6.5147781E-1, -1.5059801E+2, -1.3746760E+4,  2.1801632E+0,
                9.4306384E-1,  1.2279898E+2, -1.1839435E+4, -1.0668773E-1 });
        }
    }
}

void update_time_data(const double t)
{
  int T_index = -1;
  for(int i = 1; i < t_T.size(); ++i)
    {
      if(t < t_T[i])
        {
          T_index = i;
          break;
        }
    }

  int W_index = -1;
  for(int i = 1; i < t_W.size(); ++i)
    {
      if(t < t_W[i])
        {
          W_index = i;
          break;
        }
    }

  T[Nr] = T_box[T_index-1] + (t - t_T[T_index-1])/(t_T[T_index] - t_T[T_index-1]) * (T_box[T_index] - T_box[T_index-1]);
  double W_val = W_s[W_index] + (t - t_W[W_index-1])/(t_W[W_index] - t_W[W_index-1]) * (W_s[W_index] - W_s[W_index-1]);
  
  for(int i = 0; i <= Nr; ++i)
    {
      if(alpha[i] > 0.5)
        {
          W[i] = W_val;
        }
      else
        {
          W[i] = 0;
        }
    }
}

double Cp_calc(const std::vector<double> & dat)
{
  // C_gas = R_gas*(a1 + a2*Tg + a3*Tg^2 + a4*Tg^3 + a5*Tg^4)
  if(dat[0]>dat[1])
    return dat[2]*(dat[3] + dat[0]*(dat[4] + dat[0]*(dat[5] + dat[0]*(dat[6] + dat[0]*dat[7]))));
  else
    return dat[2]*(dat[8] + dat[0]*(dat[9] + dat[0]*(dat[10] + dat[0]*(dat[11] + dat[0]*dat[12]))));
}

double lambda_calc(const std::vector<double> & dat)
{
  // lambda_gas = T^A*exp(B/T + C/T^2 + D)
  if(dat[0]>dat[1])
    return 1e-4*pow(dat[0],dat[2])*exp(dat[3]/dat[0] + dat[4]/(dat[0]*dat[0]) + dat[5]);
  else
    return 1e-4*pow(dat[0],dat[6])*exp(dat[7]/dat[0] + dat[8]/(dat[0]*dat[0]) + dat[9]);
}

