#ifndef _IMPORT_H_
#define _IMPORT_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "constants.h"
#include "variables.h"

void import(std::vector<double> & t, std::vector<double> & x, const char path[])
{
  std::string line;
  std::stringstream ss;
  
  std::ifstream datafile;
  datafile.open(path, std::ios::in);
  if(datafile.is_open())
  {
    // Skip first line
    getline(datafile,line);

    while(true)
      {
        getline(datafile,line);

        if(!datafile.good())
          {
            break;
          }
        
        ss << line;

        double temp;
        ss >> temp;
        t.push_back(temp);

        ss >> temp;
        x.push_back(temp);

        ss.str( std::string() );
        ss.clear();
      }

    datafile.close();
  }
  else
    {
      std::cout << "Error opening file: " << path << "\n";
    }
};

void export_res(const char path[], const double t)
{
  std::fstream datafile;
  datafile.open(path, std::ios::out);
  if(datafile.is_open())
  {
    datafile << "t = " << t << "\n";
    datafile << "r,m T,C alpha" << "\n";

    for(int i = 0; i <= Nr; ++i)
      {
        const char * type = alpha[i] > 0.5 ? "pmma" : "gas";  
        datafile << r[i] << " " << T[i]-C_to_K << " " << type << "\n";
      }

    datafile.close();
  }
  else
    {
      std::cout << "Error opening file: " << path << "\n";
    }
}



#endif // _IMPORT_H_
