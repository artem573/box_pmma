#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <unordered_map>
#include <cstdlib>

class Consts
{
  std::unordered_map<std::string, std::string> _um;

 public:

  Consts(std::string file_name);
  void read_consts(std::string file_name);
  int get_int(std::string var_name) const;
  double get_double(std::string var_name) const;
};

extern const Consts consts;

// Thermophysical parameters
extern const double rho_s, lambda_s, C_s;
//extern const double rho_g, lambda_g, C_g;
extern const double W_N2, R_N2, R0;
extern const double Q_s;
extern const double p0;
extern const double C_to_K;


// Geometry
extern const double L_box, W_box, H_box;
extern const double rs0;
extern const double r_box;


// Mesh
extern const int Nr;
extern const int Ns0;
extern const double dr;

// Time
extern const double dt;
extern double dt_write;
extern double t_end;

// Other
extern const double SMALL;
extern const double LARGE;


#endif // _CONSTANTS_H_
