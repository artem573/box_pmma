#include "constants.h"

Consts::Consts(std::string file_name)
{
  read_consts(file_name);
}

void Consts::read_consts(std::string file_name)
{
  std::string line;
  std::stringstream ss;

  std::ifstream datafile;
  datafile.open(file_name, std::ios::in);
    if(!datafile.is_open())
    {
      std::cout << "Error opening file " << file_name << "\n";
        exit(1);
    }

    while(getline(datafile,line))
      {
        ss << line;
        std::string var, val;
        ss >> var;
        if(var != "" and var != "//")
          {
            ss >> val;
            _um[var] = val;
          }

        ss.str( std::string() );
        ss.clear();
      }

    datafile.close();
}

int Consts::get_int(std::string var_name) const
{
  std::string::size_type sz;

  int tmp;

  try
    {
      tmp = strtol(_um.at(var_name).c_str(),0,10);
    }
  catch(const std::out_of_range& oor)
    {
      std::cout << "There is no variable named '" << var_name << "' in the read data file \n";
      exit(1);
    }

  return tmp;
}

double Consts::get_double(std::string var_name) const
{
  std::string::size_type sz;
  double tmp;

  try
    {
      tmp = strtod(_um.at(var_name).c_str(),0);
    }
  catch(const std::out_of_range& oor)
    {
      std::cout << "There is no variable named '" << var_name << "' in the read data file \n";
      exit(1);
    }

  return tmp;
}

const Consts consts("input_data.dat");

// Physical parameters
const double R0 = 8.314;
const double W_N2 = consts.get_double("W_N2");
const double R_N2 = R0*1000.0/W_N2;

const double p0 = consts.get_double("p0");
const double Q_s = consts.get_double("Q_s");
const double rho_s = consts.get_double("rho_s");
const double lambda_s = consts.get_double("lambda_s");
const double C_s = consts.get_double("C_s");
//const double rho_g = consts.get_double("rho_g");
//const double lambda_g = consts.get_double("lambda_g");
//const double C_g = consts.get_double("C_g");
const double C_to_K = 273.15;

// Geometry
const double rs0 = consts.get_double("rs0");
const double L_box = consts.get_double("L_box");
const double H_box = consts.get_double("H_box");
const double W_box = consts.get_double("W_box");
const double r_box = pow(L_box*H_box*W_box / (4.0/3*3.14), 1.0/3);

// Mesh
const int Nr = consts.get_int("Nr");
const int Ns0 = Nr*rs0/r_box;
const double dr = r_box/Nr;

// Time
const double dt = consts.get_double("dt");
double dt_write = consts.get_double("dt_write");
double t_end = consts.get_double("t_end");

// Other
const double SMALL = 1e-15;
const double LARGE = 1e20;
