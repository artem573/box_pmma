FILES = main.cpp constants.cpp variables.cpp solve.cpp

all:
	g++ $(FILES) -o pmma -std=c++11
		
static:
	g++ $(FILES) -o pmma -std=c++11 -static

debug:
	g++ $(FILES) -o pmma -std=c++11 -g

clean:
	rm -rf *.o pmma
