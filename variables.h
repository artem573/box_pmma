#ifndef _VARIABLES_H_
#define _VARIABLES_H_

#include <vector>

extern double us;
extern double rs;
extern int Ns;

extern std::vector<double> r, r_f;
extern std::vector<double> alpha;

extern std::vector<double> rho, C, lambda, T, T0;

extern std::vector<double> t_T;
extern std::vector<double> T_box;
extern std::vector<double> t_W;
extern std::vector<double> W_s;

extern std::vector<double> Q, W;

extern std::vector<double> a, b, c, d;
extern std::vector<double> lambda_f;


#endif // _VARIABLES_H_
