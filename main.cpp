#include <vector>
#include "import.h"
#include "constants.h"
#include "variables.h"
#include "solve.h"

int main()
{
  std::ofstream T_out("results_T.dat");

  import(t_T, T_box, "T_box.dat");
  import(t_W, W_s,   "W_s.dat");

  for(auto & it: T_box)
    {
      it += C_to_K;
    }

  t_end > t_T.back() ? t_end = t_T.back() : 0;
  t_end > t_W.back() ? t_end = t_W.back() : 0;
  
  double t = 0;
  t < t_T[0] ? t = t_T[0] : 0;
  t < t_W[0] ? t = t_W[0] : 0;

  dt_write = dt_write < dt ? 1.01*dt : dt_write;
  double t_write = t;
  init();

  for(;;)
    {
      t += dt;
      if(t > t_end)
        {
          break;
        }
      
      // Store old field
      for(unsigned int i = 0; i < T.size(); ++i)
        {
          T0[i] = T[i];
        }

      update_thermo();
      update_time_data(t);

      us = rs/3*W[0];
      rs -= us*dt;
      Ns = Nr*rs/r_box;
      rs < 0 ? rs = 0 : 0;
      for(int i = 0; i <= Nr; ++i)
        {
          if(rs < r[i])
            {
              alpha[i] = 0;
            }
        }

	  if(t > t_write)
	    {
		  t_write = t + dt_write;
			
	      T_out << "t=" << t << "s \r\n";
		  T_out << "r,m ";
          for(int i = 0; i <= Ns; ++i)
            {
              T_out << r[i] << " ";
            }
          T_out << "\r\n" << "T,C ";

          for(int i = 0; i <= Ns; ++i)
            {
              T_out << T[i]-C_to_K << " ";
            }
          T_out << "\r\n \r\n";
		}

      
      interp_lambda();
      set_coeffs();
      progonka();
    }

  //export_res("results/data.dat", t);
  T_out.close();
  
  return 0;
}
