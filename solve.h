#ifndef _SOLVE_H_
#define _SOLVE_H_

#include <math.h>
#include <vector>
#include "constants.h"
#include "variables.h"


void init();
void update_thermo();
double Cp_calc(const std::vector<double> & dat);
double lambda_calc(const std::vector<double> & dat);
void update_time_data(const double t);

void interp_lambda();
void set_coeffs();
void progonka();
double sqr(const double a);

#endif // _SOLVE_H_
